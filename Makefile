
CFLAGS = -Wall -g -O2
LDFLAGS = -g

all : hello

hello : hello.o
hello.o : hello.c

.PHONY: clean
clean: 
	rm -f hello *.o

.PHONY: run
run: hello
	./hello

